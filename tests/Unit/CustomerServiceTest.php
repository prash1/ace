<?php

namespace Tests\Unit;

use App\Repositories\Repository;
use App\ServiceCustomer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerServiceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $data = [
            'name' => 'asdf',
            'email' => 'asdf',
            'tag_no' => '123',
            'phone_number'=> 'asdfa',
            'vin_number'=>'asdfasdf34534',
            'license_plate'=>  'asdfa'
        ];

        $serviceRepo = new Repository(new ServiceCustomer);
        $customer = $serviceRepo->create($data);

        $this->assertInstanceOf(ServiceCustomer::class, $customer);
        $this->assertEquals($data['name'], $customer->name);
        $this->assertEquals($data['email'], $customer->email);
        $this->assertEquals($data['tag_no'], $customer->tag_no);
    }
}
