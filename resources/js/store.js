import { getLocalUser } from "./_services/login.service";
import { getServiceCustomer } from "./_helpers/general";

const user = getLocalUser();
//const customer = getServiceCustomer();
export default{
    state: {
        currentUser:user,
        isLoggedIn: !!user,
        loading: false,
        auth_error:null,
        serviceCustomer:[],
        serviceTrackers:[],
        customers:null,
        services:null,
        modalFlag:false
    },
    getters: {
        isLoading(state){
            return state.loading;
        },
        isLoggedIn(state){
            return state.isLoggedIn;
        },
        currentUser(state){
            return state.currentUser;
        },
        authError(state){
            return state.auth_error;
        },
        scustomers(state){
            return state.serviceCustomer;
        },
        getServices(state){
            return state.services;
        },
        getServiceTrackers(state){
            return state.serviceTrackers;
        },
        getModalFlag(state){
            return state.modalFlag;
        }
    },
    mutations: {
        login(state){
            state.loading=true;
            state.auth_error=null;

        },
        loginSuccess(state,payload){
            state.auth_error=null;
            state.isLoggedIn=true;
            state.loading = false;
            state.currentUser = Object.assign({},payload.user,{token:payload.access_token});

            localStorage.setItem("user",JSON.stringify(state.currentUser));

        },
        loginFailed(state,payload){
            state.loading=false;
            state.auth_error = payload.error;
        },
        logout(state){
            localStorage.removeItem("user");
            state.isLoggedIn=false;
            state.currentUser=null;
        },
        SET_CUSTOMERS (state, payload) {
            state.serviceCustomer = payload;
        },
        SET_SERVICETRACKER(state, payload) {
            state.serviceTrackers = payload;
        },
        SET_SERVICES(state,payload){
            state.services = payload;
        },
        setModalFlag(state,payload){
            state.modalFlag=payload.status;
        }
    },
    actions: {
        login(context){
            context.commit('login');
        },
        createCustomer(context){
            context.commit('CREATE_CUSTOMER');
        },
        loadCustomers ({ commit }) {
            axios
                .get('api/service-customer')
                .then(response => response.data)
                .then(customers => {
                    commit('SET_CUSTOMERS', customers.data)
                })
        },
        loadServiceTracker ({ commit, state }) {
            state.loading=true;
            //console.log(state.currentUser);
            axios
                .get('api/service-tracker')
                .then(response => response.data)
                .then(res => {
                    state.loading=false;
                    commit('SET_SERVICETRACKER', res.data)
                })
        },
        loadServiceTypes ({ commit }) {
            axios
                .get('api/service-type')
                .then(response => response.data)
                .then(service => {
                    commit('SET_SERVICES', service.data)
                })
        },
        saveService(context,formData) {
            context.state.loading=true;
            axios
              .post("api/service-tracker", formData)
              .then(response => {
                context.state.loading=false;
              });
          },
          updateService(context,formData) {
              context.state.loading=true;
              axios
                .put("api/service-tracker/"+formData.id, formData)
                .then(response => {
                  context.state.loading=false;
                });
            }
        
    }
}