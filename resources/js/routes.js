import Login from './pages/Login/index.vue';
import Dashboard from './pages/Dashboard/index.vue';
import ServiceCustomer from './pages/ServiceCustomer/index.vue';
import ServiceBoard from './pages/ServiceBoard/index.vue';
import ArchieveList from './pages/ArchieveList/index.vue';
import ServiceStarted from './pages/ServiceStarted/index.vue';

export const routes = [{
        path: '/',
        component: Login
    },
    {
        path: '/dashboard',
        component: Dashboard,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/service-customer',
        component: ServiceCustomer,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/service-archive',
        component: ArchieveList,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/service-board',
        component: ServiceBoard,
    }, {
        path: '/service-started',
        component: ServiceStarted,
        meta: {
            requiresAuth: true
        }
    }
];
