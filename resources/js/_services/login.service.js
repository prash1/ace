import Axios from "axios";
import { setAuthorization } from "../_helpers/general";


export function login(credentials){
    return new Promise((res,rej)=> {
        Axios.post('api/login',credentials)
            .then((response) => {
                setAuthorization({token: response.data.access_token});
                res(response.data);
            })
            .catch((err) => {
                rej("Wrong email or password!!!");
            });
    });
};

export function getLocalUser(){
    const userStr = localStorage.getItem("user");
    if(!userStr){
        return null;
    }else {
        return JSON.parse(userStr)
    }
};