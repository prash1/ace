import Axios from "axios";

export function initialize(store, router){
    router.beforeResolve((to, from, next) => {
        // If this isn't an initial page load.
        if (to.name) {
            // Start the route progress bar.
            NProgress.start()
        }
        next()
    })

    router.afterEach((to, from) => {
        // Complete the animation of the route progress bar.
        NProgress.done()
    })

    router.beforeEach((to,from,next)=>{
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const currentUser = store.state.auth.currentUser;

        if(requiresAuth && !currentUser){
            next('/');
        } else if(to.path == '/' && currentUser){
            next('/dashboard');
        } else {
            next();
        }

    });
    
    axios.interceptors.response.use(null,(error)=>{
        if(error.response.status == 401){
            store.commit("auth/LOGOUT");
            store.push('/');
        }
    });
    if (store.getters.currentUser) {
        setAuthorization(store.getters.currentUser);
    }
};

export function setAuthorization(user) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${user.token}`
} 



export function createServiceCustomer(formData){
    return new Promise((res,rej)=> {
        Axios.post('api/service-customer',formData)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};

export function getIp(){
    return new Promise((res,rej)=> {
        Axios.get('//ip-api.com/json')
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't get Ip");
            });
    });
};

export function getWeather(city,code){
    return new Promise((res,rej)=> {
        let url = '/api/getweather?city='+city+'&code='+code;
        Axios.get(url)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't get Ip");
            });
    });
};

export function readyToPickUp(){
    return new Promise((res,rej)=> {
        let url = '/api/readyToPickUp';
        Axios.get(url)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't get Ip");
            });
    });
};


export function getWeatherSecure(lat,lng){
    return new Promise((res,rej)=> {
        let url = '/api/getweatherSecure?lat='+lat+'&lng='+lng;
        Axios.get(url)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't get Ip");
            });
    });
};


