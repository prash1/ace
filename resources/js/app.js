require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import {
    routes
} from './routes';
import MainApp from './components/MainApp.vue';
import Nav from './components/Nav.vue';

import 'vue-toastr-2/dist/vue-toastr-2.min.css'
import auth from './_store/auth';
import customers from './_store/customers';
import servicetracker from './_store/servicetracker';
import servicetype from './_store/servicetype';
import serviceboard from './_store/serviceboard';
import archieveservice from './_store/archieveservice';
import serviceStart from './_store/servicestart';
import loading from './_store/utilities/loading';
import alert from './_store/utilities/alert';
import search from './_store/utilities/search';


import {
    initialize
} from './_helpers/general';
import Vuelidate from 'vuelidate'
import VueMoment from 'vue-moment';
import Autocomplete from 'vuejs-auto-complete'
import BootstrapVue from 'bootstrap-vue'
import VueCountdown from '@chenfengyuan/vue-countdown';

import VueBarcode from '@chenfengyuan/vue-barcode';
import VueHtmlToPaper from 'vue-html-to-paper';

import VueToastr2 from 'vue-toastr-2'
import VueWait from 'vue-wait';

Vue.use(VueWait);

Vue.component(VueCountdown.name, VueCountdown);
Vue.use(VueRouter);
Vue.use(Vuelidate)


Vue.use(BootstrapVue);
Vue.use(VueMoment);
Vue.use(Autocomplete);

window.toastr = require('toastr')

Vue.use(VueToastr2)

Vue.component(VueBarcode.name, VueBarcode);
const options = {
    name: '_blank',
    specs: [
        'fullscreen=yes',
        'titlebar=yes',
        'scrollbars=yes'
    ],
    height: 90,
    width: 41,
    styles: [
        'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
        'https://unpkg.com/kidlat-css/css/kidlat.css'
    ]
}
Vue.use(VueHtmlToPaper, options);

window.Event = new Vue();


Vue.use(Vuex)
const store = new Vuex.Store({
    modules: {
        auth,
        customers,
        servicetracker,
        servicetype,
        loading,
        alert,
        search,
        serviceboard,
        archieveservice,
        serviceStart
    }
});


const router = new VueRouter({
    routes,
    mode: "hash"
});

initialize(store, router);

const app = new Vue({
    el: '#app',
    router: router,
    store: store,
    components: {
        MainApp,
        Nav
    },
    wait: new VueWait({
        // Defaults values are following:
        useVuex: true, // Uses Vuex to manage wait state
        vuexModuleName: 'wait', // Vuex module name

        registerComponent: true, // Registers `v-wait` component
        componentName: 'v-wait', // <v-wait> component name, you can set `my-loader` etc.

        registerDirective: true, // Registers `v-wait` directive
        directiveName: 'wait', // <span v-wait /> directive name, you can set `my-loader` etc.
        accessorName: '$wait'
    }),
});
