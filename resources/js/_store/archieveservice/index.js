import actions from './actions';
import getters from './getters';
import mutations from './mutations'

const state = {
    serviceArchieveList:[]
};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};