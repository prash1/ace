import {
    getArchieveServiceTrackerList,
    getArchievePaginate
} from './_api'

const getArchieveService = (context) => {
    getArchieveServiceTrackerList()
        .then((response) => {
            context.commit('SET_ARCHIEVE_SERVICE', response);

        }).
    then(() => {

    }).
    finally(() => {

    })
};

const getArchievePaginateList = (context,payload) => {
    getArchievePaginate(payload)
        .then((response) => {
            context.commit('RESET_ARCHIEVE_SERVICE');
            context.commit('SET_ARCHIEVE_SERVICE', response);

        }).
    then(() => {

    }).
    finally(() => {

    })
};


export default {
    getArchieveService,
    getArchievePaginateList
};
