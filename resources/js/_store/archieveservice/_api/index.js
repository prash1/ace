import Axios from "axios";

export function getArchieveServiceTrackerList(){
    return new Promise((res,rej)=> {
        Axios.get('api/service-archieve')
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};
export function getArchievePaginate(payload){
    return new Promise((res,rej)=> {
        Axios.get('api/service-archieve?page='+payload)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};
