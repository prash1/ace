const SET_ARCHIEVE_SERVICE = (state,payload) => { //payload ie message
    state.serviceArchieveList = payload;
};

const RESET_ARCHIEVE_SERVICE = (state) => { //payload ie message
    state.serviceArchieveList = [];
};

export default {
    SET_ARCHIEVE_SERVICE,
    RESET_ARCHIEVE_SERVICE
};