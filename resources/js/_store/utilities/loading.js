
const state = {
    isLoading:false,
    loaderName:''
};

const getters = {
    isLoading: state => state.isLoading,
    getLoaderName:state=>state.loaderName
};

const mutations = {

    SET_LOADING_FLAG (state,payload) {
        state.isLoading = payload.flag;
        state.loaderName = payload.name;
    },
    
}

const actions = {
    setFlag ({ commit}, status) {
        commit('SET_LOADING_FLAG', status);
    }
}


export default {
    state,
    getters,
    actions,
    mutations
};
