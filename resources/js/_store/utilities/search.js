const state = {
    searchParameters:{}
};

const getters = {
    getSearchParameters:state=>state.searchParameters
};

const mutations = {
    SET_SEARCH_PARAMETERS(state,payload){
        console.log("SET_SEARCH_PARAMETERS->payload",payload)
        state.searchParameters = payload;
    }
};

export default {
    state,
    getters,
    mutations
}