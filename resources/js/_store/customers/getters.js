const getCustomers = state => state.serviceCustomers;
const getCustomer = state => state.serviceCustomer;
const getCustomersList = state => state.serviceCustomersList;
const getCustomersListPaginate = state => state.serviceCustomersListPagination;
const getProcessFlag = state => state.processFlag;

export default {
    getCustomers,
    getProcessFlag,
    getCustomersList,
    getCustomer,
    getCustomersListPaginate
};
