
const SET_CUSTOMERS = (state,payload) => { //payload ie message

    state.serviceCustomers = payload;
};
const SET_CUSTOMER = (state,payload) => { //payload ie message

    state.serviceCustomer = payload;
};

const SET_CUSTOMERS_LIST = (state,payload) => { //payload ie message

    state.serviceCustomersList = payload;
};
const SET_CUSTOMERS_LIST_PAGINATE = (state,payload) => { //payload ie message

    state.serviceCustomersListPagination = payload;
};

const SET_PROCESSING_FLAG = (state,payload) => { //payload ie message
    state.processFlag = payload;
};

const PUSH_CUSTOMERS = (state,payload) => { //payload ie message

    state.serviceCustomers.push(payload);
};

export default {
    SET_CUSTOMERS,
    PUSH_CUSTOMERS,
    SET_PROCESSING_FLAG,
    SET_CUSTOMERS_LIST_PAGINATE,
    SET_CUSTOMERS_LIST,
    SET_CUSTOMER
};
