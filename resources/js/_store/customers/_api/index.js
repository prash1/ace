import Axios from "axios";

export function createServiceCustomer(formData){
    return new Promise((res,rej)=> {
        Axios.post('api/service-customer',formData)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};

export function deleteServiceCustomer(formData){
    return new Promise((res,rej)=> {
        Axios.delete('api/service-customer/'+formData.id)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};


export function updateServiceCustomer(formData){
    return new Promise((res,rej)=> {
        Axios.put('api/service-customer/'+formData.id,formData)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};

export function getCustomersList(formData){
    let searchParameter={};
    if(!formData.page)
    {
        searchParameter.page='1';
    }
    else{
        searchParameter.page=formData.page;
    }
    return new Promise((res,rej)=> {
        Axios.get('api/service-customer?page='+searchParameter.page)
            .then((response) => {
                console.log(response);
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't load data");
            });
    });
};

export function getCustomersListGetDataApi(){
    return new Promise((res,rej)=> {
        Axios.get('api/service-customer/getAll')
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't load data");
            });
    });
};

export function searchCustomerTrackers(formData){
    return new Promise((res,rej)=> {
        let url = '/api/search/customer';
        let searchParameter={};

        if(!formData.page)
        {
            searchParameter.page='1';
        }
        else{
            searchParameter.page=formData.page;
        }
        if(formData.searchText)
        {
            searchParameter.ServiceCustomerName=formData.searchText;
        }
        Axios.post(url,searchParameter)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't get Ip");
            });
    });
};
