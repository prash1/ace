import {
    createServiceCustomer,
    deleteServiceCustomer,
    getCustomersList,
    getCustomersListGetDataApi,
    searchCustomerTrackers,
    updateServiceCustomer
} from './_api'
import {searchServiceTrackers} from "../servicetracker/_api";


const createCustomer = (context, customer) => {
    context.dispatch('wait/start', 'createCustomerLoading', {
        root: true
    });
    context.commit("SET_PROCESSING_FLAG",true);
    createServiceCustomer(customer)
        .then((response) => {
            console.log(response);
            //commit('MESSAGES_UPDATED', response);
            context.commit('PUSH_CUSTOMERS', response.data);
            context.commit('SET_CUSTOMER', response.data);
        })
        .catch((error) => {
            console.error(error);
        })
        .finally(()=>{
            context.dispatch("customers/getCustomers", 1,{root:true});
            context.dispatch('wait/end', 'createCustomerLoading', {
                root: true
            });
            context.commit("SET_PROCESSING_FLAG",false);
        });
}

const editCustomer = ({commit,dispatch}, customer) => {
    context.dispatch('wait/start', 'updateCustomerLoading', {
        root: true
    });
    let updateStatus =  customer.updateStatus;
    updateServiceCustomer(customer)
        .then((response) => {
            if (updateStatus == 0) {
                commit('PUSH_CUSTOMERS', response.data);
            }
            dispatch("getCustomers");
            dispatch('alert/success', "Customer Updated Successfully.",{root:true});
        })
        .catch((error) => {
            console.error(error);
        })
        .finally(() => {
            context.dispatch('wait/end', 'updateCustomerLoading', {
                root: true
            });
        });
}

const deleteCustomer = (context, customer) => {
    deleteServiceCustomer(customer)
        .then((response) => {
            context.dispatch("getCustomers");
            context.dispatch('alert/success', "Customer Deleted Successfully.",{root:true});
        })
        .catch((error) => {
            console.error(error);
        })
        .finally(() => {

        });
}


const updateCustomer = ({commit,dispatch}, customer) => {
    let updateStatus =  customer.updateStatus;
    dispatch('wait/start', 'updateCustomerLoading', {
        root: true
    });
    commit("SET_PROCESSING_FLAG",true);
    updateServiceCustomer(customer)
        .then((response) => {
            //commit('MESSAGES_UPDATED', response);
            if (updateStatus == 0) {
                commit('PUSH_CUSTOMERS', response.data);
            }
                dispatch("customers/getCustomers", 1,{root:true});
            dispatch('alert/success', "Customer Updated Successfully.",{root:true});
        })
        .catch((error) => {
            console.error(error);
        })
        .finally(() => {
                dispatch('wait/end', 'updateCustomerLoading', {
                root: true
            });
            commit("SET_PROCESSING_FLAG",false);
        });
}


const getCustomers = (context,payload) => {
    context.dispatch('wait/start', 'customerTableLoading', {
        root: true
    });
    getCustomersList(payload).then((response) => {
        context.commit('SET_CUSTOMERS', response.data)
        context.commit('SET_CUSTOMERS_LIST_PAGINATE', response)
    }).
    finally(() => {
        context.dispatch('wait/end', 'customerTableLoading', {
            root: true
        })
    })
};

const getCustomersListApi = (context) => {
    context.dispatch('wait/start', 'customerTableLoading', {
        root: true
    });
    getCustomersListGetDataApi().then((response) => {
        context.commit('SET_CUSTOMERS_LIST', response.data)
    }).
    finally(() => {
        context.dispatch('wait/end', 'customerTableLoading', {
            root: true
        })
    })
};


const getCustomersSearchResult = (context, payload) => {
    context.dispatch('wait/start', 'customerTableLoading', {
        root: true
    });
    let resultSize=[];
    searchCustomerTrackers(payload)
        .then((response) => {
           console.log(response);
            context.commit('SET_CUSTOMERS', response.data.data);
            context.commit('SET_CUSTOMERS_LIST_PAGINATE', response.data)

        }).
    then(() => {

    }).
    finally(() => {
        context.dispatch('wait/end', 'customerTableLoading', {
            root: true
        })
    })
};

export default {
    getCustomers,
    createCustomer,
    updateCustomer,
    getCustomersListApi,
    getCustomersSearchResult,
    deleteCustomer
};
