import actions from './actions';
import getters from './getters';
import mutations from './mutations'

const state = {
    serviceBoardList:[],
    ip:'',
    weatherDetails:[],
    readyToPick:[],
};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};