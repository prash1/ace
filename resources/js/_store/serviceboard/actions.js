import { getServiceBoardList,getWeatherDetailApi,getReadyToPickApi } from './_api'


const getServiceBoardListData = (context,payload) => {
  getServiceBoardList(payload).then((response) => {
        context.commit('SET_SERVICE_BOARD_LIST', response)
    })
};

const getWeatherDetails = (context) => {
  getWeatherDetailApi().then((response) => {
        context.commit('SET_WEATHER_DETAILS', response.data)
    })
};

const getReadyToPick = (context) => {
  getReadyToPickApi().then((response) => {
        context.commit('SET_READY_TO_PICK_UP', response.data)
    })
};


export default {
  getServiceBoardListData,
  getReadyToPick,
  getWeatherDetails
};
