
const SET_SERVICE_BOARD_LIST = (state,payload) => { //payload ie message
    
    state.serviceBoardList = payload;
};


const SET_WEATHER_DETAILS = (state,payload) => { //payload ie message

    state.weatherDetails.push(payload);
}

const SET_READY_TO_PICK_UP = (state,payload) => { //payload ie message

    state.getReadyToPick.push(payload);
};

export default {
    SET_SERVICE_BOARD_LIST,
    SET_WEATHER_DETAILS,
    SET_READY_TO_PICK_UP
};