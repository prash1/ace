const getServiceBoardList = state => state.serviceBoardList;
const getWeatherDetails = state => state.weatherDetails;
const getIp = state => state.ip;
const getReadyToPick = state => state.readyToPick;

export default {
    getServiceBoardList,
    getWeatherDetails,
    getReadyToPick,
    getIp
};
