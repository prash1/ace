import Axios from "axios";

export function getServiceBoardList(payload){
  return new Promise((res,rej)=> {
    Axios.get('api/service-board'+'?page='+payload)
        .then((response) => {
          res(response.data);
        })
        .catch((err) => {
          rej("Couldn't load data");
        });
  });
};

export function getWeatherDetailApi(){
  return new Promise((res,rej)=> {
    Axios.get('api/service-customer')
        .then((response) => {
          res(response.data);
        })
        .catch((err) => {
          rej("Couldn't load data");
        });
  });
};

export function getReadyToPickApi(){
  return new Promise((res,rej)=> {
    Axios.get('api/service-customer')
        .then((response) => {
          res(response.data);
        })
        .catch((err) => {
          rej("Couldn't load data");
        });
  });
};
