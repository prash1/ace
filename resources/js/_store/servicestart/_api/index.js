import Axios from "axios";


export function updateService(serviceId) {
    return new Promise((res, rej) => {
        Axios.post('api/service_customer/get', {
                "tagNo": serviceId
            })
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};
