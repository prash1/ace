import {
    updateService
} from './_api'

const state = {
    serviceStatus: 0,
    processFlag: false
};
const getters = {
    getProcessFlag: state => state.processFlag
}
const mutations = {
    SET_PROCESSING_FLAG(state, payload) {
        state.processFlag = payload;
    }
}
const actions = {
    updateService({
        commit,
        dispatch
    }, serviceId) {

        dispatch('wait/start', 'updateService', {
            root: true
        });
        commit("SET_PROCESSING_FLAG", true);
        updateService(serviceId)
            .then((response) => {
                dispatch('alert/success', "Service Updated Successfully.", {
                    root: true
                });
            })
            .catch((error) => {
                console.error(error);
            })
            .finally(() => {
                dispatch('wait/end', 'updateService', {
                    root: true
                });
                commit("SET_PROCESSING_FLAG", false);
            });
    }
}
export default {
    state,
    actions,
    getters,
    mutations,
};
