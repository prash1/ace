const SET_SERVICE_TRACKERS = (state,payload) => { //payload ie message
    state.serviceTrackers = payload;
};

const SET_SERVICE_TRACKER_PAGINATION = (state,payload) => { //payload ie message
    state.serviceTrackersPagination = payload;
};
const SET_PROCESSING_FLAG = (state,payload) => { //payload ie message
    state.processFlag = payload;
};

const SET_SERVICE_TRACKER = (state,payload) => { //payload ie message
    state.serviceTracker = [];
    state.serviceTracker.push(payload);
};
export default {
    SET_SERVICE_TRACKER,
    SET_SERVICE_TRACKER_PAGINATION,
    SET_SERVICE_TRACKERS,
    SET_PROCESSING_FLAG
};