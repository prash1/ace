const getServiceTrackers = state => state.serviceTrackers;
const getServiceTracker = state => state.serviceTracker;
const getProcessFlag = state => state.processFlag;
export default {
    getServiceTrackers,
    getServiceTracker,
    getProcessFlag
};
