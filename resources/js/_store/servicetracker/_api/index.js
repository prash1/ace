import Axios from "axios";

export function createService(formData){

    return new Promise((res,rej)=> {
        Axios.post('api/service-tracker',formData)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};

export function updateService(formData){

    return new Promise((res,rej)=> {
        Axios.put('api/service-tracker/'+formData.id,formData)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};


export function getServiceTrackerList(){
    return new Promise((res,rej)=> {
        Axios.get('api/service-tracker')
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};


export function getServiceTrackerPagination(payload){
    return new Promise((res,rej)=> {
        Axios.get('api/service-tracker?page='+payload)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't Save");
            });
    });
};

export function searchServiceTrackers(formData){
    return new Promise((res,rej)=> {
        let url = '/api/search';
        let searchParameter={};

        if(!formData.page)
        {
            searchParameter.page='1';
        }
        else{
            searchParameter.page=formData.page;
        }
        if(formData.searchText)
        {
            searchParameter.customerName=formData.searchText;
            //searchParameter.customerVehicle=formData.searchText;
        }
        if(formData.searchType)
        {
            searchParameter.type=formData.searchType;
        }
        if(formData.searchDate.length>0)
        {
            searchParameter.date=formData.searchDate;
        }

        Axios.post(url,searchParameter)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't get Ip");
            });
    });
};


export function searchBarcodeTag(tagNo){
    return new Promise((res,rej)=> {
        let url = '/api/search/service_customer';
        let searchParameter={
            tagNo:tagNo
        };


        Axios.post(url,searchParameter)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't get Ip");
            });
    });
};

export function deleteServices(formData){
    return new Promise((res,rej)=> {
        let url = '/api/service-tracker/'+formData.id;
        Axios.delete(url)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Couldn't get Data");
            });
    });
};
