import {
    createService,
    getServiceTrackerList,
    updateService,
    getServiceTrackerPagination,
    searchServiceTrackers,
    searchBarcodeTag,
    deleteServices
} from './_api'

const createServiceTracker = (context, serviceData) => {
    context.dispatch('wait/start', 'createServiceLoading', {
        root: true
    });
    context.commit("SET_PROCESSING_FLAG",true);
    createService(serviceData)
        .then((response) => {

            context.dispatch('alert/success', "Service Created Successfully.",{root:true});

        })
        .catch((error) => {
            // eslint-disable-next-line
            console.error(error);
        })
        .finally(() => {
            context.dispatch('wait/end', 'createServiceLoading', {
                root: true
            });
          var  searchParameter= {
            searchText:'',
            searchType:'',
            searchDate:[],
            waiterName: 'serviceTableRefreshing'
          }
          context.dispatch("getServiceTrackersSearchResult", searchParameter);
            context.commit("SET_PROCESSING_FLAG",false);
        });
}

const updateServiceTracker = (context, serviceData) => {
    context.dispatch('wait/start', 'updateServiceLoading', {
        root: true
    });
    context.commit("SET_PROCESSING_FLAG",true);
    updateService(serviceData)
        .then((response) => {
            var  searchParameter= {
                searchText:'',
                searchType:'',
                searchDate:[],
                waiterName: 'serviceTableRefreshing'
              }
            // context.dispatch("getServiceTrackersSearchResult", searchParameter);
            context.dispatch('alert/success', "Service Updated Successfully.",{root:true});
        })
        .catch((error) => {

            console.error(error);
        }).
    finally(() => {
        context.dispatch('wait/end', 'updateServiceLoading', {
            root: true
        });
        context.commit("SET_PROCESSING_FLAG",false);
    });
}

const deleteServiceTracker = (context, serviceData) => {
  context.dispatch('wait/start', 'updateServiceLoading', {
    root: true
  });
  context.commit("SET_PROCESSING_FLAG",true);
  deleteServices(serviceData)
      .then((response) => {
        var  searchParameter= {
          searchText:'',
          searchType:'',
          searchDate:[],
          waiterName: 'serviceTableRefreshing'
        }
        context.dispatch("getServiceTrackersSearchResult", searchParameter);
        context.dispatch('alert/success', "Service Deleted Successfully.",{root:true});
      })
      .catch((error) => {

        console.error(error);
      }).
  finally(() => {
    context.dispatch('wait/end', 'updateServiceLoading', {
      root: true
    });
    context.commit("SET_PROCESSING_FLAG",false);
  });
}

const getServiceTrackers = (context, payload) => {
    let waiterName = "serviceTableLoading"
    if (payload && payload.waiterName) {
        waiterName = payload.waiterName;
    }
    context.dispatch('wait/start', waiterName, {
        root: true
    });
    var resultSize=0;
    getServiceTrackerList()
        .then((response) => {
            resultSize=response.data.length;
            if(resultSize>0)
            response.data[0]._rowVariant = 'highlight';
            context.commit('SET_SERVICE_TRACKERS', response.data);
            context.commit('SET_SERVICE_TRACKER_PAGINATION', response)

        }).
    then(() => {

        if(resultSize>0)
        {
            let serviceTrackersList = context.getters["getServiceTrackers"];
            context.commit("SET_SERVICE_TRACKER", serviceTrackersList[0]);
            window.Event.$emit("rowClickHandler");
        }
    }).
    finally(() => {
        context.dispatch('wait/end', waiterName, {
            root: true
        })
    })
};

const getServiceTrackersSearchResult = (context, payload) => {
    let waiterName = "serviceTableLoading"
    if (payload && payload.waiterName) {
        waiterName = payload.waiterName;
    }
    context.dispatch('wait/start', waiterName, {
        root: true
    });
    let resultSize=[];
    searchServiceTrackers(payload)
        .then((response) => {
            resultSize=response.data.length;
            if(resultSize>0)
            response.data[0]._rowVariant = 'highlight';
            context.commit('SET_SERVICE_TRACKERS', response.data);
            context.commit('SET_SERVICE_TRACKER_PAGINATION', response)

        }).
    then(() => {
        if(resultSize>0)
        {
            let serviceTrackersList = context.getters["getServiceTrackers"];
            context.commit("SET_SERVICE_TRACKER", serviceTrackersList[0]);
            window.Event.$emit("rowClickHandler");
        }
    }).
    finally(() => {
        context.dispatch('wait/end', waiterName, {
            root: true
        })
    })
};

const searchSeriveByBarcodeTag = (context, payload) => {
    context.dispatch('wait/start', 'searchScannedLoading', {
        root: true
    });
    searchBarcodeTag(payload)
        .then((response) => {
          console.log(response);
            if(response.success)
                context.commit('SET_SERVICE_TRACKER', response.data);
            else
                context.dispatch('alert/error', "Sorry!! no data found.",{root:true});

        }).
    finally(() => {
        context.dispatch('wait/end', 'searchScannedLoading', {
            root: true
        });
    })
};

const getServiceTrackerPaginationList = (context,payload) => {

    getServiceTrackerPagination(payload)
        .then((response) => {
            if(response.data.length>0)
                context.commit('SET_SERVICE_TRACKER', response.data[0]);
            else
                context.dispatch('alert/error', "Sorry!! no data found.",{root:true});
            context.commit('SET_SERVICE_TRACKER_PAGINATION', response); // define a state and set this pagination data to it and use it respectively in list.
        })


};

export default {
    getServiceTrackers,
    createServiceTracker,
    updateServiceTracker,
    getServiceTrackersSearchResult,
    getServiceTrackerPaginationList,
    searchSeriveByBarcodeTag,
    deleteServiceTracker
};
