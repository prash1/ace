const getServiceTypes = state => state.serviceTypes;

export default {
    getServiceTypes,
};
