import { serviceTypeList } from './_api'

const getServiceTypes = (context) => {

    serviceTypeList()
        .then((response) => {
        context.commit('SET_SERVICE_TYPE', response.data);
    })


};
export default {
    getServiceTypes
};
