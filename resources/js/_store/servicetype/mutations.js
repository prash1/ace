const SET_SERVICE_TYPE = (state,payload) => { //payload ie message
    state.serviceTypes = payload;
};

export default {
    SET_SERVICE_TYPE
};