import Axios from "axios";

export function serviceTypeList() {
    return new Promise((res, rej) => {
        axios.get('api/service-type')
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("Could not get list");
            });
    });
};
