
const isLoggedIn = state => state.isLoggedIn;
const currentUser = state => state.currentUser;
const authError = state => state.auth_error;

export default {
    isLoggedIn,
    currentUser,
    authError
};
