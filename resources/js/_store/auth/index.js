import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import { getLocalUser } from "../../_services/login.service";

const user = getLocalUser();

const state = {
    currentUser:user,
    isLoggedIn: !!user,
    auth_error:null,
};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};