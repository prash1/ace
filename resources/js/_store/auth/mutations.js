
const LOGIN = (state) => {
    state.auth_error=null;
};
const LOGIN_SUCCESS = (state,payload) => { //payload ie message
    state.auth_error=null;
    state.isLoggedIn=true;
    state.currentUser = Object.assign({},payload.user,{token:payload.access_token});
    localStorage.setItem("user",JSON.stringify(state.currentUser));
};

const LOGIN_FAILED = (state,payload) => { //payload ie message
    state.auth_error = payload.error;
};

const LOGOUT = (state,payload) => { //payload ie message
    localStorage.removeItem("user");
    state.isLoggedIn=false;
    state.currentUser=null;
};

export default {
    LOGIN,
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    LOGOUT
};