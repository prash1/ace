<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <style type="text/css">
        body{
            font-family: \'Roboto\', sans-serif;
        }
        #wrapper{
            width: 100%;
            max-width: 600px;
            margin: 0 auto;
        }

        #header{
            background: url("'.$url.'/images/service_tracking_email/HeaderImage.png");
            width: 100%;
            height: 150px;
            padding: 20px 0 30px;
        }

        #header h4 {
            color: #000000;
            font-size: 30px;
            font-weight: 500;
            text-transform: uppercase;
        }


        #content h5 {
            color: #666666;
            font-size: 24px;
            font-weight: 700;
        }

        #content h6 {
            color: #04a7e0;
            font-size: 30px;
            font-weight: 700;
        }

        #content p {
            color: #666666;
            font-family: Roboto;
            font-size: 16px;
            font-weight: 400;
        }

        #footer{
            background: #f3f3f3;
            height: 150px;
            width: 100%;
            padding: 20px 0 22px;
        }

        #footer p{
            color: #999999;
            font-size: 11px;
            font-weight: 400;
        }

        .text-center{
            text-align: center;
        }

        .top_30{
            margin-top: 30px;
        }

        #footer_social_icons li{
            display: inline-block;
            margin:20px 3px;
            list-style: none;
        }
    </style>
</head>
<body>

<div id="wrapper"  class="text-center">
    <div id="header">
        <img src="{{ asset('images/service_tracking_email/BMW_Logo.png') }}" alt="" />
        <h4 class="top_30">Policaro BMW Service Update</h4>
    </div><!-- #header -->

    <div id="content">
        <h5 class="top_30">Hello {{ $customer_name }} </h5>
        <p>{{ $sms_message }}</p>
        {!! $etaHtml  !!}
    </div><!-- #content -->

    <div id="footer">

        <img src="{{ asset('images/service_tracking_email/QL_Logo.png') }}" alt="" />
        <p>4789 Yonge Street Suite 807, Toronto, ON M2N 0G3<br />
            Terms and Conditions | Privacy Policy</p>

        <ul id="footer_social_icons">
            <li><a><img src="'.$url.'/images/service_tracking_email/social-facebook.png" alt="" /></a></li>
            <li><a><img src="'.$url.'/images/service_tracking_email/social-gplus.png" alt="" /></a></li>
            <li><a><img src="'.$url.'/images/service_tracking_email/social-twitter.png" alt="" /></a></li>
            <li><a><img src="'.$url.'/images/service_tracking_email/social-linkedin.png" alt="" /></a></li>
        </ul>
    </div><!-- #footer -->


</div><!-- #wrapper -->
</body>
</html>