<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'APILoginController@login');

Route::middleware('jwt.auth')->get('users', function () {
    return auth('api')->user();
});

//Route::group(['middleware'=>'jwt.auth'], function(){
    Route::get('service-tracker/getAll', 'ServiceTrackerController@getAllWithPagination');
    Route::get('service-customer/getAll', 'ServiceCustomerController@getAllCustomerList');
    Route::apiResource('service-customer', 'ServiceCustomerController');
    Route::apiResource('service-tracker', 'ServiceTrackerController');
    Route::apiResource('service-type', 'ServiceTypeController');
    Route::apiResource('service-board', 'ServiceBoardController');
    Route::apiResource('service-archieve', 'API\ServiceTrackerArchieveController');
    Route::get('getweather', 'ServiceBoardController@getWeather');
    Route::get('getweatherSecure', 'ServiceBoardController@getWeatherSecure');
    Route::get('readyToPickUp', 'ServiceBoardController@readyToPickUp');
    Route::post('search', 'SearchController@filter');
    Route::post('search/customer', 'SearchController@filterServiceCustomer');
    Route::post('search/service_customer', 'ServiceTrackerController@getServiceCustomer');
    Route::post('/service_customer/get', 'ServiceTrackerController@getServiceCustomerBarcode');
//});
