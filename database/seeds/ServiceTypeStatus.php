<?php

use Illuminate\Database\Seeder;

class ServiceTypeStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_type_statuses')->insert([

            [
                'service_type_id' => 1,
                'service_status' => 'Checked In',
                'service_message' => 'This is a message from [Dealership]. Your vehicle has been checked in and will enter in the workshop shortly. An advisor will be in touch with you shortly.',
                'e_time'=>0
            ],
            [
                'service_type_id' => 1,
                'service_status' => 'Service Started',
                'service_message' => 'A technician has begun service on your vehicle. Your estimated time of completion has been updated to:',
                'e_time'=>90
            ],
            [
                'service_type_id' => 1,
                'service_status' => 'Service Updated',
                'service_message' => 'There is a delay on your service. Your new estimated time of completion is [ETA]. We apologize for the inconvenience.',
                'e_time'=>0
            ],
            [
                'service_type_id' => 1,
                'service_status' => 'Additional Services',
                'service_message' => 'Additional Services Additional services have been added to your work order. Your estimated time of completion has been updated to:  ',
                'e_time'=>0
            ],
            [
                'service_type_id' => 1,
                'service_status' => 'Maintenance Completed',
                'service_message' => 'Your vehicle service has now been completed and will enter the car wash shortly. Your estimated time of completion has been updated to:',
                'e_time'=>15
            ],
            [
                'service_type_id' => 1,
                'service_status' => 'Pick up Ready',
                'service_message' => 'The car wash on your vehicle has now been completed. Please contact your advisor or service concierge to coordinate pick up arrangements at this number: 647 727 8792',
                'e_time'=>0
            ],

            [
                'service_type_id' => 2,
                'service_status' => 'Checked In',
                'service_message' => 'This is a message from [Dealership]. Your vehicle has been checked in and will enter in the workshop shortly. An advisor will be in touch with you shortly.',
                'e_time'=>0
            ],
            [
                'service_type_id' => 2,
                'service_status' => 'Diagnostic Started',
                'service_message' => 'A technician has begun a diagnostic review on your vehicle. Your estimated time of completion has been updated to:',
                'e_time'=>90
            ],
            [
                'service_type_id' => 2,
                'service_status' => 'Service Started',
                'service_message' => 'A technician has begun service on your vehicle. Your estimated time of completion has been updated to:',
                'e_time'=>0
            ],
            [
                'service_type_id' => 2,
                'service_status' => 'Warranty Delayed',
                'service_message' => 'Your vehicle is awaiting warranty approval.',
                'e_time'=>0
            ],
            [
                'service_type_id' => 2,
                'service_status' => 'Parts on Order',
                'service_message' => 'Parts have been ordered for your service. Your estimated time of completion has been updated to:',
                'e_time'=>0
            ],
            [
                'service_type_id' => 2,
                'service_status' => 'Service Completed',
                'service_message' => 'Your vehicle service has now been completed and will enter the car wash shortly. Your estimated time of completion has been updated to:',
                'e_time'=>15
            ],

        ]);
    }
}
