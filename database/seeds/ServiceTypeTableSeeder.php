<?php

use Illuminate\Database\Seeder;

class ServiceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_types')->insert([

            [
                'type' => 'Express',
            ],
            [
                'type' => 'Diagnostic',
            ],

        ]);
    }
}
