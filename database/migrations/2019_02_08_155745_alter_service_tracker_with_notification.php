<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServiceTrackerWithNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_trackers', function (Blueprint $table) {
            $table->boolean('email_notification')->default(0);
            $table->boolean('sms_notification')->default(0);
            $table->boolean('eta_notification')->default(0);
            $table->text('additional_notes')->nullable();
            $table->text('message_template');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_trackers', function (Blueprint $table) {
            $table->dropColumn('email_notification');
            $table->dropColumn('sms_notification');
            $table->dropColumn('eta_notification');
            $table->dropColumn('additional_notes');
            $table->dropColumn('message_template');
        });
    }
}
