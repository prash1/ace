<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_trackers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_customer_id')->unsigned();
            $table->integer('service_type_id')->unsigned();
            $table->integer('service_type_status_id')->unsigned();
            $table->string('service_time');
            $table->string('service_time_unit');
            $table->integer('created_by')->unsigned()->nullable();
            $table->dateTime('service_created_at');
            $table->foreign('created_by')->references('id')->on('users');
            $table->integer('tag_no');
            $table->foreign('service_customer_id')->references('id')->on('service_customers')->onDelete('cascade');
            $table->foreign('service_type_id')->references('id')->on('service_types')->onDelete('cascade');
            $table->foreign('service_type_status_id')->references('id')->on('service_type_statuses')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_trackers');
    }
}
