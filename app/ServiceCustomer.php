<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceCustomer extends Model
{

    // use SoftDeletes;
    protected $table='service_customers';

    protected $fillable=[
        'name',
        'email',
        'tag_no',
        'phone_number',
        'vin_number',
        'license_plate',
        'created_by',
    ];

    public function User()
    {
        return $this->belongsTo('App\User','created_by');
    }
}
