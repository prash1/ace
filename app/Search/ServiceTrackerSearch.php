<?php

namespace App\Search;

use App\ServiceTracker;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\Builder;

class ServiceTrackerSearch
{
    public static function apply(Request $filters,$paginate=false,$perPage=8)
    {
        $query = 
            static::applyDecoratorsFromRequest(
                $filters, (new ServiceTracker)->newQuery()
            );
        $query = $query->where('status',0);
        if ($paginate){
            return static::paginateResults($query,$perPage);
        }else{
            return static::getResults($query);
        }
    }
    
    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
    {

        foreach ($request->all() as $filterName => $value) {

           if ($filterName != 'page'){
               $decorator = static::createFilterDecorator($filterName);

               if (static::isValidDecorator($decorator)) {
                   $query = $decorator::apply($query, $value);
               }
           }

        }
        return $query;
    }
    
    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\Filters\\' .
            str_replace(' ', '', 
                ucwords(str_replace('_', ' ', $name)));
    }
    
    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query)
    {
        return $query->get();
    }

    private static function paginateResults(Builder $query, $perPage=8)
    {
        return $query->paginate($perPage);
    }

}