<?php

namespace App\Search\Filters;

use Illuminate\Database\Eloquent\Builder;

class CustomerVehicle implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->whereHas('ServiceCustomer', function ($query) use ($value) {
            $query->where('vin_number', 'LIKE', '%' . $value . '%');
        })->with('ServiceCustomer','ServiceType','ServiceTypeStatus');
    }
}