<?php

namespace App\Search\Filters;

use Illuminate\Database\Eloquent\Builder;

class CustomerName implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->whereHas('ServiceCustomer', function ($query) use ($value) {
            $query->where('name', 'LIKE', '%' . $value . '%');
        })->with('ServiceCustomer','ServiceType','ServiceTypeStatus');
    }
}