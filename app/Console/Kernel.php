<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\ServiceTracker;
use DB;
use Helper;
use Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $data = DB::table('service_trackers')->where('status',0)->get();
            $arrServiceList = $data->toArray();
            foreach($arrServiceList as $service){
                $createdAt = Carbon::parse($service->updated_at);
                $completed_time = $createdAt->toDateTimeString();
                $currentTime = Carbon::now();
                if($currentTime->diffInMinutes($completed_time) > 30 ){
                    $update = ServiceTracker::find($service->id);
                    $update->timestamps = false;
                    $update->status = 1;
                    $update->save();
                    $update = ServiceTracker::find($service->id);
                    $update->timestamps = true;
                    $update->save();
                }
            }
        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
