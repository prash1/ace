<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceTypeStatus extends Model
{

    protected $table='service_type_statuses';

    protected $fillable=[
        'service_type_id',
        'service_status',
        'service_message',
        'e_time'
    ];
}
