<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceTracker extends Model
{

     use SoftDeletes;
//    protected $table='service_trackers';

    protected $fillable=[
        'service_customer_id',
        'service_type_id',
        'service_type_status_id',
        'service_time',
        'service_time_unit',
        'email_notification',
        'sms_notification',
        'eta_notification',
        'additional_notes',
        'message_template',
        'tag_no',
        'status',
        'created_by',
        'service_created_at'
    ];

    public function ServiceType()
    {
        return $this->belongsTo('App\ServiceType');
    }

    public function ServiceCustomer()
    {
        return $this->belongsTo('App\ServiceCustomer');
    }

    public function ServiceTypeStatus()
    {
        return $this->belongsTo('App\ServiceTypeStatus','service_type_status_id');
    }
}
