<?php

namespace App\Mail;
use App\ServiceTracker;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ServiceTrackerMail extends Mailable
{
    use Queueable, SerializesModels;

    public $servicetracker;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceTracker $serviceTracker)
    {
        $this->servicetracker = $serviceTracker;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $customer_email = $this->servicetracker->ServiceCustomer->email;
        $customer_name = $this->servicetracker->ServiceCustomer->name;
        $http = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
        $host = $_SERVER['HTTP_HOST'];
        $url=$http."://".$host;
        // multiple recipients
        $to  = $customer_email;
        // subject
        $subject = 'POLICARO BMW SERVICE UPDATE';
        $etaHtml = '';
        $sms_message=$this->servicetracker->ServiceTypeStatus->service_message;
        $eta=$this->servicetracker->service_time." ".$this->servicetracker->service_time_unit;
        $status = true;
        if (strpos($sms_message, '[Dealership]') !== false) {
            $sms_message = str_replace("[Dealership]",'Policaro BMW',$sms_message);
        }
        if (strpos($sms_message, '[ETA]') !== false) {
            $sms_message = str_replace("[ETA]",$eta,$sms_message);
            $status = false;
        }
        if (in_array($this->servicetracker->service_type_status_id,[1,6])) {
            $status = false;
        }else{
            $status = true;
        }
        if ($status) {
            $etaHtml = '<h6>'.$eta.'</h6>';
        }
        return $this->to($to)
                    ->subject($subject)
                    ->from('info@test.com')
                    ->view('email.servicetracker')
                    ->with([
                        'url' => $url,
                        'subject' => $subject,
                        'etaHtml' => $etaHtml,
                        'sms_message' => $sms_message,
                        'customer_name' => $customer_name,
                        'data' => $this->servicetracker,
                    ]);;
    }
}
