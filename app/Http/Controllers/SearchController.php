<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Search\ServiceTrackerSearch;
use App\Search\ServiceCustomerSearch;
use App\Http\Resources\ServiceTrackerResources as ServiceTrackerResources;
/*
 * {
        "customerName": "Avi ",
        "customerVehicle": "BMW 2001 REW",
        "type": 1,
        "date": [
                    'to'=> "2019-01-10",
                    'from'=> "2019-01-01"
                ],
    }
 * */

class SearchController extends Controller
{
    public function filter(Request $request)
    {

        $paginate = true;
        $perPage = 8;
        $serviceTrackers = ServiceTrackerSearch::apply($request,$paginate,$perPage);
        return ServiceTrackerResources::collection($serviceTrackers);
    }

    public function filterServiceCustomer(Request $request)
    {

        $paginate = true;
        $perPage = 25;
        $serviceCustomers = ServiceCustomerSearch::apply($request,$paginate,$perPage);
        return response([
            'status' =>'success',
            'data'=>$serviceCustomers
        ], 200);
    }
}
