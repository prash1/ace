<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ServiceCustomer as ServiceCustomerResource;
use App\ServiceCustomer;
use App\Repositories\Repository;
use App\Http\Requests\ServiceCustomerRequest;
use Auth;

class ServiceCustomerController extends Controller
{

    public $headerArray;
    protected $model;


    public function __construct( ServiceCustomer $serviceCustomer)
    {
        $this->headerArray = [

            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers' => 'Content-Type,x-prototype-version,x-requested-with',
        ];
        $this->model = new Repository($serviceCustomer);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {

        $serviceCustomers = $this->model->with('User')->orderBy('id','desc')->paginate(25);
        return ServiceCustomerResource::collection($serviceCustomers);
    }

    public function getAllCustomerList()
    {

        $serviceCustomers = $this->model->all();
        return response([
            'status' =>'success',
            'data'=>$serviceCustomers
        ], 200);
//            return ServiceCustomerResource::collection($serviceCustomers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceCustomerRequest $request)
    {
        $validated = $request->validated();
        $data = $request->all();
        $post = [
            'name' => $data['name'],
            'email' => $data['email'],
            'tag_no' =>  '123',
            'phone_number'=>  $data['phone_number'],
            'vin_number'=> $data['vin_number'],
            'license_plate'=>  $data['license_plate']
        ];
        try {
            $serviceCustomer = $this->model->create($post);
        } catch (QueryException $e) {

        }


        return new ServiceCustomerResource($serviceCustomer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceCustomer $serviceCustomer)
    {
        return new ServiceCustomerResource($serviceCustomer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $serviceTrackersUpdate = ServiceCustomer::find($id)->update($request->only(['name', 'email','phone_number','vin_number','license_plate']));
        $serviceTrackers = ServiceCustomer::find($id);
        return new ServiceTrackerResources($serviceTrackers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceCustomer $serviceCustomer)
    {
        $serviceCustomer->update($request->only(['name', 'email','tag_no','phone_number','vin_number','license_plate']));

        return new ServiceCustomerResource($serviceCustomer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceCustomer $serviceCustomer)
    {
        $serviceCustomer->delete();

        return response()->json(null, 204);
    }
}
