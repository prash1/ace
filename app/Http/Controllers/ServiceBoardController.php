<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ServiceTrackerResources as ServiceTrackerResources;
use App\ServiceTracker;
use Auth;
use Carbon\Carbon;

class ServiceBoardController extends Controller
{
    public $headerArray;

    public function __construct()
    {
        $this->headerArray = [

            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers' => 'Content-Type,x-prototype-version,x-requested-with',
        ];

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from_date=Carbon::yesterday();
        $to_date=Carbon::now()->addDay();
        $value = 1;//which service to show in service board for now its Express and 2 for Diagnostic
        $serviceTrackers = ServiceTracker::with('ServiceType','ServiceCustomer','ServiceTypeStatus')
            ->whereBetween('created_at', [$from_date, $to_date])
            ->where('status',0)
            ->where('service_type_id',$value)
            ->paginate(8);


        return ServiceTrackerResources::collection($serviceTrackers);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function readyToPickUp()
    {
        $yesterday = Carbon::yesterday();
        $today = Carbon::now()->addDay();


        $service_list = ServiceTracker::with('ServiceType','ServiceCustomer','ServiceTypeStatus')->where('status',0)->where([
            'service_type_status_id' => 6
        ])->orWhere([
            'service_type_status_id' => 12
        ])->whereBetween('created_at', [$yesterday, $today])->orderBy('updated_at', 'DESC')->first();

        return response([
            'status' => 'success',
            'data'=>$service_list,

        ], 200);
    }

    public function getWeather(){
        $weatherLink = '';
        if( isset($_SERVER['HTTPS'] ) ) {
            $weatherLink = 'https://api.openweathermap.org/data/2.5/weather?lat=';
            $get_data=$weatherLink.$_GET['lat']."&lon=".$_GET['lng']."&appid=84c63aefd10267808c549d06022b30e5";
        }else{
            $weatherLink = 'http://api.openweathermap.org/data/2.5/weather?q=';
            $get_data=$weatherLink.$_GET['city'].",".$_GET['code']."&appid=84c63aefd10267808c549d06022b30e5";
        }



        $json = file_get_contents($get_data);

        //decode JSON to array
        $data = json_decode($json,true);


        $description=$data['weather'][0]['main'];
        $weather_icons=$data['weather'][0]['icon'];

        $temp=$data['main']['temp'];
        $temp = $temp-273.15;
        $today_date = date("F d, Y");



        $datas = [];
        $datas['description'] = $description;
        $datas['temp'] = round($temp);
        $datas['weather_icons'] = $weather_icons;
        $datas['today_date'] = $today_date;
        $datas['city'] = $_GET['city'];




        return response([
            'status' => 'success',
            'data'=>$datas,

        ], 200);
    }

    public function getWeatherSecure(){
        $weatherLink = '';

            $weatherLink = 'https://api.openweathermap.org/data/2.5/weather?lat=';
            $get_data=$weatherLink.$_GET['lat']."&lon=".$_GET['lng']."&appid=84c63aefd10267808c549d06022b30e5";

        $json = file_get_contents($get_data);

        //decode JSON to array
        $data = json_decode($json,true);


        $description=$data['weather'][0]['main'];
        $weather_icons=$data['weather'][0]['icon'];

        $temp=$data['main']['temp'];
        $temp = $temp-273.15;
        $today_date = date("F d, Y");



        $datas = [];
        $datas['description'] = $description;
        $datas['temp'] = $temp;
        $datas['weather_icons'] = $weather_icons;
        $datas['today_date'] = $today_date;
        $datas['city'] =$data['name'];
        $datas['datas'] = $data;




        return response([
            'status' => 'success',
            'data'=>$datas,

        ], 200);
    }
}
