<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ServiceTrackerResources as ServiceTrackerResources;
use App\ServiceTracker;
use App\Mail\ServiceTrackerMail;
use App\Http\Resources\sendMsg as sendMsg;
use App\Http\Transformations\ServiceTrackertransformation;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;
use Carbon\Carbon;

class ServiceTrackerController extends Controller
{
    use ServiceTrackertransformation;

    public function getServiceCustomer(Request $request)
    {

        $serviceTrackers = ServiceTracker::with('ServiceType','ServiceCustomer','ServiceTypeStatus')->find((int)$request->tagNo);
        if ($serviceTrackers != null){

            return response()->json([
                'success' => true,
                'data'=>$this->transformService($serviceTrackers)
            ]);
        }else{
            return response()->json([
                'success' => false,
                'data'=>[]
            ]);
        }
    }
    public function getServiceCustomerBarcode(Request $request)
    {
        $data = [
            'service_type_id'=>1,
            'service_type_status_id'=>2,
            'status'=>0
        ];
        $serviceTrackersUpdate = ServiceTracker::find((int)$request->tagNo)->update($data);
        $serviceTrackers = ServiceTracker::with('ServiceType','ServiceCustomer','ServiceTypeStatus')->find((int)$request->tagNo);
        if ($serviceTrackers != null){
            $this->sendNotifications($serviceTrackers->id,$serviceTrackers);
            return response()->json([
                'success' => true,
                'data'=>$this->transformService($serviceTrackers)
            ]);
        }else{
            return response()->json([
                'success' => false,
                'data'=>[]
            ]);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from_date=Carbon::yesterday();
        $to_date=Carbon::now()->addDay();

//        dd($from_date, $to_date);
        $serviceTrackers = ServiceTracker::with('ServiceType','ServiceCustomer','ServiceTypeStatus')
                                            ->whereBetween('created_at', [$from_date, $to_date])
                                            ->where('status',0)
                                            ->paginate(5);


        return ServiceTrackerResources::collection($serviceTrackers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $serviceTrackers = new ServiceTracker();
        $serviceTrackers->service_customer_id = $request->service_customer_id;
        $serviceTrackers->service_type_id = $request->service_type_id;
        $serviceTrackers->service_type_status_id = $request->service_type_status_id;
        $serviceTrackers->service_time = $request->service_time;
        $serviceTrackers->service_time_unit = $request->service_time_unit;
        $serviceTrackers->tag_no = $request->tag_no;
        $serviceTrackers->email_notification = $request->email_notification;
        $serviceTrackers->sms_notification = $request->sms_notification;
        $serviceTrackers->eta_notification = $request->eta_notification;
        $serviceTrackers->additional_notes = $request->additional_notes;
        $serviceTrackers->status = 0;
        $serviceTrackers->message_template = $request->service_message;
        $serviceTrackers->save();
        $this->sendNotifications($serviceTrackers->id,$serviceTrackers);

        return new ServiceTrackerResources($serviceTrackers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $serviceTrackers = ServiceTracker::find($id);
        return ServiceTrackerResources::collection($serviceTrackers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $serviceTrackersUpdate = ServiceTracker::find($id)->update($request->only(['message_template','additional_notes','eta_notification','email_notification','sms_notification','service_time', 'service_type_id','service_type_status_id','service_time_unit','tag_no']));
        $serviceTrackers = ServiceTracker::find($id);
        $serviceTrackers->status = 0;
        $serviceTrackers->save();
        $this->sendNotifications($id,$serviceTrackers);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = ServiceTracker::find($id)->delete();
        return response([
            'status' => 'success',
            'data'=>$delete

        ], 200);
    }

    public function sendNotifications($serviceTrackerId,$serviceTrackers){

        $data =  ServiceTracker::with('ServiceType','ServiceCustomer','ServiceTypeStatus')->find($serviceTrackerId);
        $customer_phone= $data->ServiceCustomer->phone_number;
        $customer_email= $data->ServiceCustomer->email;
        $sms_message=$data->message_template;
        $eta=$data->service_time." ".$data->service_time_unit;
        $status = true;
        if (strpos($sms_message, '[Dealership]') !== false) {
            $sms_message = str_replace("[Dealership]",'Policaro BMW',$sms_message);
        }
        if (strpos($sms_message, '[ETA]') !== false) {
            $sms_message = str_replace("[ETA]",$eta,$sms_message);
            $status = false;
        }
        if (in_array($data->service_type_status_id,[1,6]) || $data->eta_notification) {
            $status = false;
        }else{
            $status = true;
        }
        $checkStatus = 0;
        if ($data->sms_notification == 1){
            try {
                $msg=new sendMsg();
                $msg->sendMessage($customer_phone,$sms_message,$eta,$status);
            } catch(\Exception $exception){
//                $checkStatus = 1;
            }
        }

        if ($data->email_notification == 1){
            try {
                Mail::to($customer_email)->send(new ServiceTrackerMail($data));
            } catch(\Exception $exception){
//                $checkStatus = 1;
            }
        }

        if ($checkStatus == 0){
            return new ServiceTrackerResources($serviceTrackers);
        }else{
            return response()->json([
                'success'=>true,
                'message' =>'email or sms is invalid'
            ],200);
        }
    }

    public function getAllWithPagination(){

        $from_date=Carbon::yesterday();
        $to_date=Carbon::now()->addDay();
        $serviceTrackers = ServiceTracker::with('ServiceType','ServiceCustomer','ServiceTypeStatus')
            ->whereBetween('created_at', [$from_date, $to_date])
            ->paginate(8);
        return response([
            'status' => 'success',
            'paginate'=>$serviceTrackers

        ], 200);
    }
}
