<?php

namespace App\Http\Transformations;

use App\ServiceTracker;
use Carbon\Carbon;
trait ServiceTrackertransformation
{
    /**
     * Transform the address
     *
     * @param ServiceTracker $serviceTracker
     *
     * @return array
     *
     */
    public function transformService(ServiceTracker $serviceTracker)
    {
        $createdAt = $serviceTracker->updated_at;
        $now = Carbon::now();
        if ($serviceTracker->service_time_unit == 'hr') {
            $createdAt->addHours($serviceTracker->service_time);
            $diff = $now->diffInSeconds($createdAt);
            $chkdata = $now->diffForHumans($createdAt);
        }else{
            $createdAt->addMinutes($serviceTracker->service_time);
            $diff = $now->diffInSeconds($createdAt);
            $chkdata = $now->diffForHumans($createdAt);
        }

        if (strpos($chkdata, 'before') !== false) {
            $chk = true;
        }else{
            $chk = false;
        }

        $service_time_w_u=(int)$diff;
        $diff = $diff*1000;
        $service_time_w_u_c=(int)$diff;
//dd($serviceTracker);
        return [
            'id'=>$serviceTracker->id,
            'tag' => $serviceTracker->tag_no,
            'tag2' => $serviceTracker->tag_no,
            'name'=> $serviceTracker->ServiceCustomer->name,
            'name2'=> $serviceTracker->ServiceCustomer->name,
            'license_plate'=>$serviceTracker->ServiceCustomer->license_plate,
            'license_plate2'=>$serviceTracker->ServiceCustomer->license_plate,
            'vin_number2' => $serviceTracker->ServiceCustomer->vin_number,
            'vin_number' => $serviceTracker->ServiceCustomer->vin_number,
            'type' =>$serviceTracker->ServiceType->type,
            'type2' =>$serviceTracker->ServiceType->type,
            'status'=>$serviceTracker->ServiceTypeStatus->service_status,
            'status2'=>$serviceTracker->ServiceTypeStatus->service_status,
            'eta'=>$serviceTracker->service_time.$serviceTracker->service_time_unit,
            'service_type_id'=>$serviceTracker->service_type_id,
            'service_type_status_id'=>$serviceTracker->service_type_status_id,
            'service_time'=>$serviceTracker->service_time,
            'service_time_unit'=>$serviceTracker->service_time_unit,
            'service_message'=>$serviceTracker->ServiceTypeStatus->service_message,
            'email'=>$serviceTracker->ServiceCustomer->email,
            'phone_number'=>$serviceTracker->ServiceCustomer->phone_number,
            'service_customer_id'=>$serviceTracker->ServiceCustomer->id,
            'chk'=>$chk,
            'service_time_w_u_c'=>$service_time_w_u_c,
            'service_time_w_u'=>$service_time_w_u,
            'email_notification'=>$serviceTracker->email_notification,
            'sms_notification'=>$serviceTracker->sms_notification,
            'eta_notification'=>$serviceTracker->eta_notification,
            'additional_notes'=>$serviceTracker->additional_notes,
            'message_template'=>$serviceTracker->message_template,
        ];
    }
}
