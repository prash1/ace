<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:service_customers',
            'phone_number' => 'required',
            'vin_number' => 'required',
            'license_plate' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Full name is required',
            'name.alpha' => 'Full name must alphabet only.',
            'email.required' => 'Email is required',
            'email.email' => 'Invalid Email',
            'phone_number.required' => 'Phone Number is required',
            'vin_number.required' => 'Vin number is Required.',
            'license_plate.required' => ' License plate is Required.',
        ];
    }


}
