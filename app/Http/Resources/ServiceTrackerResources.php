<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ServiceTrackerResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
            $createdAt = $this->updated_at;
            $now = Carbon::now();
            if ($this->service_time_unit == 'hr') {
                $createdAt->addHours($this->service_time);
                $diff = $now->diffInSeconds($createdAt);
                $chkdata = $now->diffForHumans($createdAt);
            }else{
                $createdAt->addMinutes($this->service_time);
                $diff = $now->diffInSeconds($createdAt);
                $chkdata = $now->diffForHumans($createdAt);
            }

            if (strpos($chkdata, 'before') !== false) {
                $chk = true;
            }else{
                $chk = false;
            }

            $service_time_w_u=(int)$diff;
            $diff = $diff*1000;
            $service_time_w_u_c=(int)$diff;

            return [
                'id'=>$this->id,
                'tag' => $this->tag_no,
                'tag2' => $this->tag_no,
                'name'=> $this->ServiceCustomer->name,
                'name2'=> $this->ServiceCustomer->name,
                'license_plate'=>$this->ServiceCustomer->license_plate,
                'license_plate2'=>$this->ServiceCustomer->license_plate,
                'vin_number2' => $this->ServiceCustomer->vin_number,
                'vin_number' => $this->ServiceCustomer->vin_number,
                'type' =>$this->ServiceType->type,
                'type2' =>$this->ServiceType->type,
                'status'=>$this->ServiceTypeStatus->service_status,
                'status2'=>$this->ServiceTypeStatus->service_status,
                'eta'=>$this->service_time.$this->service_time_unit,
                'service_type_id'=>$this->service_type_id,
                'service_type_status_id'=>$this->service_type_status_id,
                'service_time'=>$this->service_time,
                'service_time_unit'=>$this->service_time_unit,
                'service_message'=>$this->ServiceTypeStatus->service_message,
                'email'=>$this->ServiceCustomer->email,
                'phone_number'=>$this->ServiceCustomer->phone_number,
                'service_customer_id'=>$this->ServiceCustomer->id,
                'chk'=>$chk,
                'service_time_w_u_c'=>$service_time_w_u_c,
                'service_time_w_u'=>$service_time_w_u,
                'email_notification'=>$this->email_notification,
                'sms_notification'=>$this->sms_notification,
                'eta_notification'=>$this->eta_notification,
                'additional_notes'=>$this->additional_notes,
                'message_template'=>$this->message_template,
            ];
    }
}
