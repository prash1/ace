<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceCustomer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'tag_no' => $this->tag_no,
            'phone_number'=> $this->phone_number,
            'vin_number'=> $this->vin_number,
            'license_plate'=> $this->license_plate
        ];
    }
}
