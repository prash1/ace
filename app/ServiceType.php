<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    protected $table='service_types';

    protected $fillable=[
        'type'
    ];

    public function ServiceTypeStatus()
    {
        return $this->hasMany('App\ServiceTypeStatus','service_type_id');
    }
}
